#!/usr/bin/env python
# coding=utf-8

dataset = []

def rsi(period):
  dslen = len(dataset)
  P = 0
  N = 0
  for i in range(dslen):
    if (i==0): pass
    KF = dataset[i][1] - dataset[i-1][1] 
    if (KF > 0): P += KF
    else: N += KF * -1
    if(i>period+1):
      KFE = dataset[i-period][1] - dataset[i-period-1][1]
      if(KFE > 0): P -= KFE
      else: N -= KFE * -1
      if( N != 0):
        RS = P / N
        RSI = 100 - (100 / ( 1 + RS ))
        print("RSI: " + RSI)
      else:
        print("RSI hesaplanamadi! " + str(P) + " * " + str(N))



with open("dataset.txt") as f:
  line = f.readline().rstrip()
  i = 0
  block = [1,1,1,1]
  while line:
    if(i == 4):
      i = 0
      dataset.append(block)
      line = f.readline().rstrip()
      pass
    block[i] = int(line)
    i += 1
    line = f.readline().rstrip()

#print(dataset)


rsi(14)

