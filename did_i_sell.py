#!/usr/bin/env python
# coding=utf-8

from binance.client import Client
import datetime
import time
import sys

import output

sys.stdout = output.Unbuffered(sys.stdout)
from get_keys import get_keys
api_public, api_secret = get_keys()
client = Client(api_public, api_secret)

def run_me():
  print("running!")
  start = datetime.datetime.now()
  i = 0
  while True:
    a = datetime.datetime.now()
    time_gecen = a - start
    print(40 * "_" + " " + str(i) + " ___ " + str(time_gecen.total_seconds()))
    i += 1
    balance = client.get_asset_balance(asset="BTC")
    if (float(balance['free']) > 0.001):
      print("Sattin")
      break
    if (check_wall("DENT", 5100100, 70100100) == 0):
      break
    b = datetime.datetime.now()
    c = b - a
    print("Seconds: " + str(c.total_seconds()))
    if (c.total_seconds() < 3):
        time.sleep(3 - c.total_seconds())
  while True:
    alarm_cal()



def check_balance(item):
    balance = client.get_asset_balance(asset=item)
    if (float(balance['free']) > 0):
        print("Satin alindi!")
        return 0
    else:
        print("Henuz alinmadi!")
    return 1

def check_wall(item, alis, satis):
  mydata = client.get_orderbook_ticker(symbol = item + "BTC")
  if (float(mydata['bidQty']) < satis):
    print("Dikkatli ol, cok satis var!    " + item)
    return 0
  if (float(mydata['askQty']) < alis):
    print("Dikkatli ol, cok alis var!   " + item)
    return 0
  print("Piyasa sakin")
  return 1



def alarm_cal():
    import playsound
    playsound.playsound('ses.mp3', True)

run_me()

